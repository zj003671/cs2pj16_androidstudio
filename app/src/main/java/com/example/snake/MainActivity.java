package com.example.snake;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static ImageView img_swipe;
    public static Dialog dialogMenu, dialogHelp, dialogLevel, dialogScoreboard;
    private GameView gv;
    RelativeLayout rl_back, rl_back_levels, rl_mode1, rl_mode2, rl_mode3;
    public static TextView txt_score, txt_best_score, txt_dialog_score, txt_dialog_best_score;
    public static EditText userInput;

    //Scoreboard
    private ArrayList<Integer> scores;
    private ArrayList<Integer> scoresOut;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private ListView scoresLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;
        setContentView(R.layout.activity_main);

        img_swipe = findViewById(R.id.img_swipe);
        gv = findViewById(R.id.gv);
        txt_score = findViewById(R.id.txt_score);
        txt_best_score = findViewById(R.id.txt_best_score);

        //Sets all the screens by calling each function.
        dialogHelp();
        dialogLevels();
        dialogScoreboard();
        dialogMenu();
    }

    /**
     * This method is used to initialise the dialog_menu.xml
     * Also allows user interaction.
     */
    private void dialogMenu() {
        int bestScore = 0;

        SharedPreferences sp = this.getSharedPreferences("gamesetting", Context.MODE_PRIVATE);
        if(sp!=null){
            bestScore = sp.getInt("bestscore",0);
        }

        dialogMenu = new Dialog(this);
        dialogMenu.setContentView(R.layout.dialog_start);
        dialogMenu.setCanceledOnTouchOutside(false);

        MainActivity.txt_best_score.setText(bestScore+"");
        txt_dialog_score = dialogMenu.findViewById(R.id.txt_dialog_score);
        txt_dialog_best_score = dialogMenu.findViewById(R.id.txt_dialog_best_score);
        txt_dialog_best_score.setText(bestScore + "");
        dialogMenu.setCanceledOnTouchOutside(false);

        dialogMenu.show();

        //Interaction of play button.
        RelativeLayout rl_start = dialogMenu.findViewById(R.id.rl_start);
        rl_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_swipe.setVisibility(View.VISIBLE);
                gv.reset();
                dialogMenu.dismiss();
            }
        });

        //Interaction of help button.
        RelativeLayout rl_help = dialogMenu.findViewById(R.id.rl_help);
        rl_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMenu.dismiss();
                dialogHelp.show();
                //startActivity(new Intent(MainActivity.this, HelpScreen.class));
            }
        });

        //Interaction of level button.
        RelativeLayout rl_level = dialogMenu.findViewById(R.id.rl_levels);
        rl_level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMenu.dismiss();
                dialogLevel.show();
            }
        });

        //Interaction of level button.
        RelativeLayout rl_scoreboard = dialogMenu.findViewById(R.id.rl_scoreboard);
        rl_scoreboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMenu.dismiss();
                dialogScoreboard.show();
            }
        });

    }

    /**
     * This method is used to initialise the dialog_help.xml
     * Also allows user interaction.
     */
    private void dialogHelp(){

        //Creates dialog for help screen, allows loading of necessary functions.
        dialogHelp = new Dialog(this);
        dialogHelp.setContentView(R.layout.dialog_help);
        dialogHelp.setCanceledOnTouchOutside(false);

        //Interaction of back button from help screen.
        rl_back = dialogHelp.findViewById(R.id.rl_back_help);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogHelp.dismiss();
                dialogMenu.show();
            }
        });
    }

    /**
     * This method is used to initialise the dialog_levels.xml
     * Also allows user interaction.
     */
    private void dialogLevels(){
        //Creates dialog for levels screen, allows loading of necessary functions.
        dialogLevel = new Dialog(this);
        dialogLevel.setContentView(R.layout.dialog_levels);
        dialogLevel.setCanceledOnTouchOutside(false);

        userInput = dialogLevel.findViewById(R.id.userInput);

        //Back Button interaction
        rl_back_levels = dialogLevel.findViewById(R.id.rl_back_levels);
        rl_back_levels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLevel.dismiss();
                dialogMenu.show();
            }
        });

        //Mode 1 interaction and setup.
        rl_mode1 = dialogLevel.findViewById(R.id.rl_level1);
        rl_mode1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameView.levelType = 0;
                gv.reset();
                dialogLevel.dismiss();
            }
        });

        //Mode 2 interaction and setup.
        rl_mode2 = dialogLevel.findViewById(R.id.rl_level2);
        rl_mode2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameView.levelType = 1;
                gv.reset();
                dialogLevel.dismiss();
            }
        });

        //Mode 3 interaction and setup.
        rl_mode3 = dialogLevel.findViewById(R.id.rl_level3);
        rl_mode3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameView.levelType = 2;
                gv.reset();
                dialogLevel.dismiss();
            }
        });
    }

    /**
     * This method is used to initialise the dialog_scoreboard.xml
     * Also allows user interaction.
     */
    private void dialogScoreboard(){

        //Creates dialog for help screen, allows loading of necessary functions.
        dialogScoreboard = new Dialog(this);
        dialogScoreboard.setContentView(R.layout.dialog_scorebaord);
        dialogScoreboard.setCanceledOnTouchOutside(false);

        // Write a message to the database
        scores = new ArrayList<>();
        scoresOut = new ArrayList<>();

        //Values sent to ListView
        ArrayAdapter<Integer> myArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        //Firebase
        database = FirebaseDatabase.getInstance();
        //Sends node to firebase for value handling.
        myRef = database.getReference("SCORES");
        myRef.setValue(scores);

        scoresLV = dialogScoreboard.findViewById(R.id.list_score);
        scoresLV.setAdapter(myArrayAdapter);

        myRef.addChildEventListener(new ChildEventListener() {
            /**
             * Value from firebase and notifies ArrayAdapter.
             * @param snapshot Single value of firebase node.
             * @param previousChildName Stores the value
             */
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                int val = snapshot.getValue(int.class);
                scoresOut.add(val);
                myArrayAdapter.notifyDataSetChanged();
            }
            /**
             * Checks the value changes in ArrayAdapter.
             * @param snapshot Single value of firebase node.
             * @param previousChildName Stores the value
             */
            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                myArrayAdapter.notifyDataSetChanged();
            }
            /**
             * Checks for the removal of firebase values.
             * @param snapshot Single value of firebase node.
             */
            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }
            /**
             * Checks for relocation of firebase values.
             * @param snapshot Single value of firebase node.
             * @param previousChildName Stores the value
             */
            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }
            /**
             * Checks for errors when handling.
             * @param error Stores error
             */
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //Interaction of back button from help screen.
        rl_back = dialogScoreboard.findViewById(R.id.rl_back_scoreboard);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogScoreboard.dismiss();
                dialogMenu.show();
            }
        });
    }
}