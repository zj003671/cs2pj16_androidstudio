package com.example.snake;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Object {
    private Bitmap bm;
    private int x, y;
    private Rect r;

    /**
     * Constructor for creating objects.
     * @param bm Bitmap to store the objects image.
     * @param x  Integer value to store object x position.
     * @param y  Integer value to store object y position.
     */
    public Object(Bitmap bm, int x, int y) {
        this.bm = bm;
        this.x = x;
        this.y = y;
    }

    /**
     * Draws object onto canvas.
     * @param canvas Canvas used to display/ paint object image..
     */
    public void draw(Canvas canvas){
        canvas.drawBitmap(bm, x, y, null);
    }

    /**
     * Changes/ Resets objects positions.
     * @param nx Integer value to store object new x position.
     * @param ny Integer value to store object new y position.
     */
    public void reset(int nx, int ny){
        this.x = nx;
        this.y = ny;
    }

    public Bitmap getBm() {
        return bm;
    }

    public void setBm(Bitmap bm) {
        this.bm = bm;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Rect getR() {
        return new Rect(this.x, this.y, this.x+GameView.sizeElementMap, this.y+GameView.sizeElementMap);
    }

    public void setR(Rect r) {
        this.r = r;
    }
}
