package com.example.snake;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Random;

public class GameView extends View {
    //Background/ game foundation for the snake to move in.
    private int w = 12, h = 21;
    public static int sizeElementMap = 75*Constants.SCREEN_WIDTH/1080;

    //Objects that are drawn in the game
    private Snake snake;
    private Object apple, bomb;
    private ArrayList<Grass> arrGrass = new ArrayList<>();
    private ArrayList<Object> obstacle = new ArrayList<>();
    //Images for each object.
    private Bitmap bmGrass1, bmGrass2, bmSnake1, bmApple, bmObstacle, bmBomb;

    //Running the game
    private Handler handler;
    private Runnable r;

    //Game variables
    private boolean move = false;
    private float mx, my;
    public static boolean isPlaying = false;
    public static int score = 0, bestScore = 0, levelType = 0;
    private Context context;

    //Sound
    private int soundEat, soundDie;
    private float volume;
    private boolean loadedsound;
    private SoundPool soundPool;

    /**
     * Constructor for GameView.
     * Initialises the bitmaps and sets images to the corresponding object bitmap.
     * Sets the background with alternating grass images.
     * Creates level based on the level type.
     * Runnable will run the java code of the game.
     * @param context Passes the context from the MainActivity.
     * @param attrs .
     */
    public GameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        SharedPreferences sp = context.getSharedPreferences("gamesetting", Context.MODE_PRIVATE);
        if(sp!=null){
            bestScore = sp.getInt("bestscore",0);
        }

        //Sets the images
        bmGrass1 = BitmapFactory.decodeResource(this.getResources(), R.drawable.grass);
        bmGrass1 = Bitmap.createScaledBitmap(bmGrass1, sizeElementMap, sizeElementMap, true);
        bmGrass2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.grass03);
        bmGrass2 = Bitmap.createScaledBitmap(bmGrass2, sizeElementMap, sizeElementMap, true);
        bmSnake1 = BitmapFactory.decodeResource(this.getResources(), R.drawable.snake1);
        bmSnake1 = Bitmap.createScaledBitmap(bmSnake1, 14*sizeElementMap, sizeElementMap, true);
        bmApple = BitmapFactory.decodeResource(this.getResources(), R.drawable.apple);
        bmApple = Bitmap.createScaledBitmap(bmApple, sizeElementMap, sizeElementMap, true);
        bmObstacle = BitmapFactory.decodeResource(this.getResources(), R.drawable.crate);
        bmObstacle = Bitmap.createScaledBitmap(bmObstacle, sizeElementMap, sizeElementMap, true);
        bmBomb = BitmapFactory.decodeResource(this.getResources(), R.drawable.bomb);
        bmBomb = Bitmap.createScaledBitmap(bmBomb, sizeElementMap, sizeElementMap, true);

        //Sets the background
        for(int i = 0; i < h; i++){
            for (int j = 0; j < w; j++){
                if((j+i)%2==0){
                    arrGrass.add(new Grass(bmGrass1, j*bmGrass1.getWidth() + Constants.SCREEN_WIDTH/2 - (w/2)*bmGrass1.getWidth(), i*bmGrass1.getHeight()+50*Constants.SCREEN_HEIGHT/1920, bmGrass1.getWidth(), bmGrass1.getHeight()));
                }else{
                    arrGrass.add(new Grass(bmGrass2, j*bmGrass2.getWidth() + Constants.SCREEN_WIDTH/2 - (w/2)*bmGrass2.getWidth(), i*bmGrass2.getHeight()+50*Constants.SCREEN_HEIGHT/1920, bmGrass2.getWidth(), bmGrass2.getHeight()));
                }
            }
        }

        //Level Creation
        createLevel();

        //Runnable and handler
        handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };

        //Sound manipulation
        if(Build.VERSION.SDK_INT>=21){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            SoundPool.Builder builder = new SoundPool.Builder();
            builder.setAudioAttributes(audioAttributes).setMaxStreams(5);
            this.soundPool = builder.build();
        }else{
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }
        this.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loadedsound = true;
            }
        });
        soundEat = this.soundPool.load(context, R.raw.eat, 1);
        soundDie = this.soundPool.load(context, R.raw.die, 1);
    }

    /**
     * Method to create level depending on the level type.
     * This is the level creation/ level modes the user can choose.
     * Initialises objects for each level.
     */
    private void createLevel(){
        //Initialises the snake.
        snake = new Snake(bmSnake1,arrGrass.get(126).getX(),arrGrass.get(126).getY(), 4);
        switch (levelType){
            case 0:
                apple = new Object(bmApple, arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
                break;
            case 1:
                apple = new Object(bmApple, arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
                bomb = new Object(bmBomb, arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
                makeRandomObstacles(6);
                break;
            case 2:
                apple = new Object(bmApple, arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
                bomb = new Object(bmBomb, arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
                try{
                    String input = String.valueOf(MainActivity.userInput.getText());
                    int temp = Integer. parseInt(input);
                    makeSetObstacles(temp);
                }catch (Exception e){
                    Log.i("Error", "No input.");
                }
                break;
        }
    }

    /**
     * Method to produce apples randomly in the game.
     * Random variable is used to set the x and y values stored in an array.
     * This will set the location of the apple.
     *
     * A check is also done when the apple intersects with the snake.
     * This increases the player score as well as increasing the snake's length.
     */
    private int[] randomApple(){
        int []xy = new int[2];
        Random r = new Random();
        xy[0] = r.nextInt(arrGrass.size()-1);
        xy[1] = r.nextInt(arrGrass.size()-1);
        Rect rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
        //Checks if the apple is being created where other objects are.
        //This will then re-draw the apple in a new position.
        if (!obstacle.isEmpty()){
            for (int i = 0; i < obstacle.size(); i++) {
                if (rect.intersect(obstacle.get(i).getR())){
                    xy[0] = r.nextInt(arrGrass.size()-1);
                    xy[1] = r.nextInt(arrGrass.size()-1);
                    rect = rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
                }
            }
        }

        //Rect rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
        boolean check = true;
        while (check){
            check = false;
            for (int i = 0; i < snake.getArrPartSnake().size(); i++){
                if(rect.intersect(snake.getArrPartSnake().get(i).getrBody())){
                    check = true;
                    xy[0] = r.nextInt(arrGrass.size()-1);
                    xy[1] = r.nextInt(arrGrass.size()-1);
                    rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
                }
            }
        }
        return xy;
    }

    /**
     * Method to create obstacles randomly in the game.
     * Random variable is used to set the x and y values stored in an array.
     * This will set the location of the apple.
     */
    private int[] randomObstacle(){
        int []xy = new int[2];
        Random r = new Random();
        Rect rect;
        xy[0] = r.nextInt(arrGrass.size()-1);
        xy[1] = r.nextInt(arrGrass.size()-1);
        rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
        return xy;
    }

    /**
     * Method to multiple obstacles in the game view.
     * The amount created and added onto the ArrayList is the argument passed.
     * But this have a range of 0-amount, therefore creating a random aspect to obstacle creation.
     * @param amount Value used to create specific amount of obstacles.
     */
    private void makeRandomObstacles(int amount){
        Random rand = new Random();
        for (int i = 0; i < rand.nextInt(amount); i++) {
            obstacle.add(new Object(bmObstacle, arrGrass.get(randomObstacle()[0]).getX(), arrGrass.get(randomObstacle()[1]).getY()));
        }
    }

    /**
     * Method to multiple obstacles in the game view.
     * The amount created and added onto the ArrayList is the argument passed.
     * @param amount Value used to create specific amount of obstacles.
     */
    private void makeSetObstacles(int amount){
        for (int i = 0; i < amount; i++) {
            obstacle.add(new Object(bmObstacle, arrGrass.get(randomObstacle()[0]).getX(), arrGrass.get(randomObstacle()[1]).getY()));
        }
    }

    /**
     * Method to create bombs at random locations in the game.
     * Random variable is used to set the x and y values stored in an array.
     * This will set the location of the apple.
     *
     * A check is also done when the bomb intersects with the snake.
     * This decreases the player score as well as decreasing the snake's length.
     */
    private int[] randomBomb(){
        int []xy = new int[2];
        Random r = new Random();
        Rect rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
        boolean check = true;
        while (check){
            check = false;
            for (int i = 0; i < snake.getArrPartSnake().size(); i++){
                if(rect.intersect(snake.getArrPartSnake().get(i).getrBody())){
                    check = true;
                    xy[0] = r.nextInt(arrGrass.size()-1);
                    xy[1] = r.nextInt(arrGrass.size()-1);
                    rect = new Rect(arrGrass.get(xy[0]).getX(), arrGrass.get(xy[1]).getY(), arrGrass.get(xy[0]).getX()+sizeElementMap, arrGrass.get(xy[1]).getY()+sizeElementMap);
                }
            }
        }
        return xy;
    }

    /**
     * In built method in Android studio.
     * The Override is used to override the foundation method.
     *
     * This method checks for the user input, this is MotionEvent.
     * Depending on what direction the user swipes their finger, the snake will move in that direction.
     * @param event Passes the context from the MainActivity.
     * @return boolean true
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int a = event.getActionMasked();
        switch (a){
            case  MotionEvent.ACTION_MOVE:{
                //Snake not moving
                if(move==false){
                    mx = event.getX();
                    my = event.getY();
                    move = true;
                }else{
                    //Checks if the user has swiped the screen more than 100 pixels.
                    //Checks if the snake is not moving right.
                    //Changes mx and my to the current mx and my of the event.
                    if(mx - event.getX() > 100 && !snake.isMove_right()){
                        mx = event.getX();
                        my = event.getY();
                        this.snake.setMove_left(true);
                        isPlaying = true;
                        MainActivity.img_swipe.setVisibility(INVISIBLE);
                    //Checks if the user has swiped the screen more than 100 pixels.
                    //Checks if the snake is not moving left.
                    //Changes mx and my to the current mx and my of the event.
                    }else if(event.getX() - mx > 100 &&!snake.isMove_left()){
                        mx = event.getX();
                        my = event.getY();
                        this.snake.setMove_right(true);
                        isPlaying = true;
                        MainActivity.img_swipe.setVisibility(INVISIBLE);
                    //Checks if the user has swiped the screen more than 100 pixels.
                    //Checks if the snake is not moving up.
                    //Changes mx and my to the current mx and my of the event.
                    }else if(event.getY() - my > 100 && !snake.isMove_up()){
                        mx = event.getX();
                        my = event.getY();
                        this.snake.setMove_down(true);
                        isPlaying = true;
                        MainActivity.img_swipe.setVisibility(INVISIBLE);
                    //Checks if the user has swiped the screen more than 100 pixels.
                    //Checks if the snake is not moving down.
                    //Changes mx and my to the current mx and my of the event.
                    }else if(my - event.getY() > 100 && !snake.isMove_down()){
                        mx = event.getX();
                        my = event.getY();
                        this.snake.setMove_up(true);
                        isPlaying = true;
                        MainActivity.img_swipe.setVisibility(INVISIBLE);
                    }
                }
                break;
            }
            //Snake not moving/ resets snake movement variables.
            case MotionEvent.ACTION_UP:{
                mx = 0;
                my = 0;
                move = false;
                break;
            }
        }
        return true;
    }

    /**
     * Draw method for the game.
     * This will call the draw method for all objects in the game.
     *
     * @param canvas Canvas argument used to draw all images and objects on screen.
     */
    public void draw(Canvas canvas){
        super.draw(canvas);
        canvas.drawColor(0xFF065700);

        //Draws the background
        for(int i = 0; i < arrGrass.size(); i++){
            canvas.drawBitmap(arrGrass.get(i).getBm(), arrGrass.get(i).getX(), arrGrass.get(i).getY(), null);
        }
        //If the boolean is true, the snake will be updated.
        if(isPlaying){
            snake.update();
            //If the snake hits the edges of the layout/background the game will be over.
            if(snake.getArrPartSnake().get(0).getX() < this.arrGrass.get(0).getX() ||snake.getArrPartSnake().get(0).getY() < this.arrGrass.get(0).getY() ||snake.getArrPartSnake().get(0).getY()+sizeElementMap>this.arrGrass.get(this.arrGrass.size()-1).getY() + sizeElementMap ||snake.getArrPartSnake().get(0).getX()+sizeElementMap>this.arrGrass.get(this.arrGrass.size()-1).getX() + sizeElementMap){
                gameOver();
            }
            //If the snake intersects with itself the game will be over.
            for (int i = 1; i < snake.getArrPartSnake().size(); i++){
                if (snake.getArrPartSnake().get(0).getrBody().intersect(snake.getArrPartSnake().get(i).getrBody())){
                    gameOver();
                }
            }
        }
        //Creates / Draws basic foundation to the game.
        snake.drawSnake(canvas);
        apple.draw(canvas);
        if (levelType > 0){
            bomb.draw(canvas);
            for (int i = 0; i < obstacle.size(); i++) {
                if (!obstacle.isEmpty()) {
                    obstacle.get(i).draw(canvas);
                }
            }
            //Intersection with bomb.
            if (snake.getArrPartSnake().get(0).getrBody().intersect(bomb.getR())) {
                bomb.reset(arrGrass.get(randomBomb()[0]).getX(), arrGrass.get(randomBomb()[1]).getY());
                if (score > 0) {
                    score--;
                }
                snake.removePart();
                scoreUpdateOnObjectInteraction();
            }
            //Intersection with crate.
            for (int i = 0; i < obstacle.size(); i++) {
                if (snake.getArrPartSnake().get(0).getrBody().intersect(obstacle.get(i).getR())) {
                    obstacle.get(i).reset(arrGrass.get(randomObstacle()[0]).getX(), arrGrass.get(randomObstacle()[1]).getY());
                    scoreUpdateOnObjectInteraction();
                    gameOver();
                }
            }
        }
        //Intersection with apple.
        if(snake.getArrPartSnake().get(0).getrBody().intersect(apple.getR())){
            if(loadedsound){
                int streamId = this.soundPool.play(this.soundEat, (float)0.5, (float)0.5, 1, 0, 1f);
            }
            apple.reset(arrGrass.get(randomApple()[0]).getX(), arrGrass.get(randomApple()[1]).getY());
            snake.addPart();
            score++;
            scoreUpdateOnObjectInteraction();
        }

        handler.postDelayed(r, 100);
    }

    /**
     * Game over method which is called to stop the game.
     * This will be represented to the user with the menu screen and updated scores.
     * A sound will play upon the game over method being called.
     */
    private void gameOver() {
        isPlaying = false;
        MainActivity.dialogMenu.show();
        MainActivity.txt_dialog_best_score.setText(bestScore+"");
        MainActivity.txt_dialog_score.setText(score+"");
        if(loadedsound){
            int streamId = this.soundPool.play(this.soundDie, (float)0.5, (float)0.5, 1, 0, 1f);
        }



    }

    /**
     * Method to update the scores on the screen.
     * This will also update the score variables.
     * This will also update the values on the main menu.
     */
    private void scoreUpdateOnObjectInteraction(){
        MainActivity.txt_score.setText(score+"");
        if(score > bestScore){
            bestScore = score;
            SharedPreferences sp = context.getSharedPreferences("SCORES", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("SCORES", bestScore);
            editor.apply();
            MainActivity.txt_best_score.setText(bestScore+"");
        }
    }

    /**
     * Method which is called to reset the game.
     * This will also reset the ArrayLists and objects.
     * This in turn reduces latency and optimises the game experience.
     */
    public void reset(){
        //Clears ArrayLists
        arrGrass.clear();
        obstacle.clear();
        //Draws the background
        for(int i = 0; i < h; i++){
            for (int j = 0; j < w; j++){
                if((j+i)%2==0){
                    arrGrass.add(new Grass(bmGrass1, j*bmGrass1.getWidth() + Constants.SCREEN_WIDTH/2 - (w/2)*bmGrass1.getWidth(), i*bmGrass1.getHeight()+50*Constants.SCREEN_HEIGHT/1920, bmGrass1.getWidth(), bmGrass1.getHeight()));
                }else{
                    arrGrass.add(new Grass(bmGrass2, j*bmGrass2.getWidth() + Constants.SCREEN_WIDTH/2 - (w/2)*bmGrass2.getWidth(), i*bmGrass2.getHeight()+50*Constants.SCREEN_HEIGHT/1920, bmGrass2.getWidth(), bmGrass2.getHeight()));
                }
            }
        }
        //Creates level based on the level type.
        createLevel();
        //Resets the score.
        score = 0;
    }
}


