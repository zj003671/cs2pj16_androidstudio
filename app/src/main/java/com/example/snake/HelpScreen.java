package com.example.snake;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

public class HelpScreen extends AppCompatActivity {

    public static Dialog dialogHelp;
    RelativeLayout rl_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;
        setContentView(R.layout.dialog_help);

        //Creates dialog for help screen, allows loading of necessary functions.
        dialogHelp = new Dialog(this);
        dialogHelp.setContentView(R.layout.dialog_help);
        dialogHelp.setCanceledOnTouchOutside(false);

        dialogHelp.show();
        dialogHelp();

    }

    /**
     * This method is used to initialise the dialog_help.xml
     * Also allows user interaction.
     */
    private void dialogHelp(){


        //Interaction of back button from help screen.
        rl_back = findViewById(R.id.rl_back_help);
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //MainActivity.dialogMenu.show();
            }
        });
    }

}
